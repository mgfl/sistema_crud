<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AlumnosSedeer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('alumnos')->insert([
            'nombre'=>'Maria',
            'apellido_paterno'=>'Flores',
            'apellido_materno'=> 'Luevanos',
            'semestre'=> 3,
            'grupo'=>'A',
        ]);
    }
}
